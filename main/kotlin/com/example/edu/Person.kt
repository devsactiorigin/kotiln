package com.example.edu

// 아래와 같이 보면 함수의 파라미터를 설정한 것처럼 보이지만,
// decompile to java 해보면, private field이고 자동으로 getter, setter를 생성해준다.
class Person(val name:String, val age: Int) {
}