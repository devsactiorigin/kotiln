package chap03.section1

fun main() {

    val name = "홍길동"
    val email = "hong@example.kr"

    // 오버로딩은 아니고, 사전에 default 파라미터 설정된 것을 활용하여 아래 2가지 선언이 모두 가능해진다.
    add(name)
    add(name, email)
    add("둘리", "dooly@example.kr")
    defaultArgs()        // 100 + 200
    defaultArgs(200)  // 200 + 200
}

fun add(name: String, email: String = "default") {
    val output = "${name}님의 이메일은 ${email}입니다."
    println(output)
}

fun defaultArgs(x: Int = 100, y: Int = 200) {
    println(x + y)
}