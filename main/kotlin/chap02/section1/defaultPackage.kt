package chap02.section1

import kotlin.math.*

fun main(){
    val intro ="안녕하세요"
    val num = 20
    val numF = 20F

    println("intro: $intro, num: $num")
    // 단축키 soutv+ctrl+space 하기
    println("intro = ${intro}")
    println("num = ${num}")
    println("numF = ${numF}")

    val pi= PI

    println("pi of kotlin.math lib = ${pi}")
}