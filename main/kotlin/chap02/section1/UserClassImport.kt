package chap02.section1

import com.example.edu.Person as User

fun main() {
    val user1 = User("kildong", 30)
    val user2 = Person("a123","kildong")

    println("print val of Person of other package")
    println("user1 = ${user1.name}")
    println("user1 = ${user1.age}")

    println("print val of Person of this file")
    println("user2 = ${user2.id}")
    println("user2 = ${user2.name}")

}

class Person(val id:String, val name:String)