package chap02.section2

fun main() {
    var a = 1
    val str1 = "a = $a"
    val str2 = "a= ${a+2}"

    println(str1)
    println(str2)

    val special = "\"hello\", i have \$15"
    println("special = ${special}")
}