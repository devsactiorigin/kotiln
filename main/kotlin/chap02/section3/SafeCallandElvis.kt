package chap02.section3

fun main() {
    // 코틀린에서는 사실상 '포인터'를 제공한다! ? 기호는 마치 int *p; 에 대응가능하다.
    // 포인터에 널값이 들어가는 것은 당연한것이고 이에 따른 함수를 제공하는 것으로 이해가능
    var str1 : String? = "hello"
    
    str1 = null
    // 애당초 null 값을 넣지 않는 것이 지향점이지만, 인생처럼 null을 넣어야하는 예외가 발생한다면
    // 그리고 이로 인해 예외처리가 필요하다면
    // ?: 를 통해 null 값이면 ; ? is null 이라면 -1을 출력하도록 아래와같이 코딩가능
    println("str1: ${str1}  length: ${str1?.length ?: -1}")
}