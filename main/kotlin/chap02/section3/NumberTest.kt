package chap02.section3

fun main() {
    // number 자료형은 일종에 int, float, double의 합으로 이해
    var test: Number =12.2
    println("test = ${test}")

    test=12
    println("test = ${test}")
    // 자료형 테스트
    ischeck(test)

    test=120L
    println("test = ${test}")
    // 자료형 테스트
    ischeck(test)

    test=12.0f
    println("test = ${test}")
    // 자료형 테스트
    ischeck(test)

}

fun ischeck(para:Number){
    if (para is Int){
        println(para)
        println("It is Int")
    } else if ( para !is Int ){
        println(para)
        println("it !is Int")
    }

}
// Any는 파이썬 변수처럼 사용가능하게 하는 자료형
fun ischeckx(para:Any){
    if (para is Int){
        println(para)
        println("It is Int")
    } else if ( para !is Int ){
        println(para)
        println("it !is Int")
    }

}