package chap02.section3

fun main() {
    // 자바는 특히 primitive 변수들 간 자동형 변환이 지원되나
    // 코틀린에서는 var 이더라라도, 아직까지는 자동형변환을 지원하지 않고 별도의 변환함수를 써야한다.

    var int_kotlin=3
    println("int_kotlin = ${int_kotlin}")
//    int_kotlin=3.5
    println("int_kotlin = ${int_kotlin}")

    // use to~()
    var Double_kotlin = int_kotlin.toDouble()
    println("Double_kotlin = ${Double_kotlin}")
    
    // == ; value 비교, === ; preference 비교
    val a = 128
    val b = 128
    val c : Int? = 128
    println(a==b)
    //// 기본 자료형의 경우 스택에 저장되기에 true
    println(a===b)
    //// Int? 의 경우, 힙저장소에 저장되어 false
    println(b===c)
    
    // ! 교재 76페이지 하단 그림 필수

}